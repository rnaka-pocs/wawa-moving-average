#!/bin/bash

echo
echo "*****************************"
echo "Building the docker API image"
echo "*****************************"
echo
./mvnw clean install -P prod dockerfile:build

echo
echo "********************************"
echo "Running mongo and API containers"
echo "********************************"
echo
docker-compose -f docker/docker-compose.yml up

