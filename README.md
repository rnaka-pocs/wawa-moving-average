# Project Title
This is a REST API which calculates the moving average of price based on multiple events.

## Getting Started

### Stack
- Java 8
- Spring Boot
- Mongodb
- Docker
- Maven

### Requirements
- Java 8
- Docker

### Installing
./mvnw clean install

Compiles and runs the tests to generate a jar file of the application.

## The API

### REST API
- POST /events 
- GET /moving-average/history

### POSTMAN
/test/MovingAverage.postman_collection.json
/test/WAWA-LOCAL.postman_environment.json


## Running the application

### Running locally
./start-locally.sh

It runs a mongodb container and the api locally

### Running containers
./start-docker-compose.sh

It builds a docker image and run docker-compose to bring up the mongodb and the api containers

### JMETER
/test/Moving Average.jmx

## HOW TO TEST

### Running the application
    * Option A: Run the containers. Command: ./start-docker-compose
    * Option B: Run the Spring Boot API with a dockerized mongo -> Command: ./start-locally.sh

### Running the tests
    * Using JMeter, open the JMeter project file from /test/Moving Average.jmx
    * Run the tests. Command: CTRL+R or button PLAY
    * Open the REPORTS thread groups
    * Check out the Listener View Results Tree