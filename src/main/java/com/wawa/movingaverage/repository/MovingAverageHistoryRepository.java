package com.wawa.movingaverage.repository;

import com.wawa.movingaverage.bean.MovingAverageHistory;
import com.wawa.movingaverage.bean.PricingModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Repository
public interface MovingAverageHistoryRepository extends MongoRepository<MovingAverageHistory, String> {

    LinkedList<MovingAverageHistory> findByCreatedOnAndPricingModel(LocalDateTime createdOn, PricingModel pricingModel);

    List<MovingAverageHistory> findByPricingModel(PricingModel pricingModel);
}
