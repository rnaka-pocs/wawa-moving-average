package com.wawa.movingaverage.repository;

import com.wawa.movingaverage.bean.MovingAverage;
import com.wawa.movingaverage.bean.PricingModel;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.repository.MongoRepository;

@Repository
public interface MovingAverageRepository extends MongoRepository<MovingAverage, String>{

    MovingAverage findByPricingModel(PricingModel pricingModel);
}
