package com.wawa.movingaverage.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.LinkedList;

@Document
public class MovingAverage {

    @Id
    private String id;
    @Indexed(unique=true)
    private PricingModel pricingModel;
    private Double average;
    private LinkedList<Double> lastPrices;

    public MovingAverage() {}

    public MovingAverage(PricingModel pricingModel, Double average) {
        this.setPricingModel(pricingModel);
        this.average = average;
    }

    public MovingAverage(String id, PricingModel pricingModel, Double average, LinkedList<Double> lastPrices) {
        this.id = id;
        this.setPricingModel(pricingModel);
        this.average = average;
        this.lastPrices = lastPrices;
    }

    public Double getAverage() {
        return average;
    }

    public void setAverage(Double average) {
        this.average = average;
    }

    public LinkedList<Double> getLastPrices() {
        return lastPrices;
    }

    public void setLastPrices(LinkedList<Double> lastPrices) {
        this.lastPrices = lastPrices;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PricingModel getPricingModel() {
        return pricingModel;
    }

    public void setPricingModel(PricingModel pricingModel) {
        this.pricingModel = pricingModel;
    }
}
