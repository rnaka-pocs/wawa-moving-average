package com.wawa.movingaverage.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Objects;

@Document
public class MovingAverageHistory {

    @Id
    private String id;
    private LocalDateTime createdOn;
    private PricingModel pricingModel;
    private Double average;

    public MovingAverageHistory() {}

    public MovingAverageHistory(String id, LocalDateTime createdOn, PricingModel pricingModel, Double average) {
        this.id = id;
        this.createdOn = createdOn;
        this.pricingModel = pricingModel;
        this.average = average;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public PricingModel getPricingModel() {
        return pricingModel;
    }

    public void setPricingModel(PricingModel pricingModel) {
        this.pricingModel = pricingModel;
    }

    public Double getAverage() {
        return average;
    }

    public void setAverage(Double average) {
        this.average = average;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (!(o instanceof MovingAverageHistory)) return false;

        MovingAverageHistory movingAverageHistory = (MovingAverageHistory)o;

        return Objects.equals(this.id, movingAverageHistory.getId())
                && Objects.equals(this.createdOn, movingAverageHistory.getCreatedOn())
                && Objects.equals(this.pricingModel, movingAverageHistory.getPricingModel())
                && Objects.equals(this.average, movingAverageHistory.getAverage());
    }
}
