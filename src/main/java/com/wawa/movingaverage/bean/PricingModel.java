package com.wawa.movingaverage.bean;

public enum PricingModel {

    STANDARD, GOLD, SILVER;

    public static PricingModel findByString(String pricingModelStr) {
        PricingModel pricingModelReturn = null;

        for (PricingModel pricingModel: PricingModel.values()) {
            if (pricingModel.name().equalsIgnoreCase(pricingModelStr)) {
                pricingModelReturn = pricingModel;
                break;
            }
        }

        return pricingModelReturn;
    }
}
