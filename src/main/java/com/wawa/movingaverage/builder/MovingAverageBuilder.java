package com.wawa.movingaverage.builder;

import com.wawa.movingaverage.bean.MovingAverage;
import com.wawa.movingaverage.bean.PricingModel;

import java.util.Collection;
import java.util.LinkedList;

public class MovingAverageBuilder {

    private String id;
    private PricingModel pricingModel;
    private Double average;
    private LinkedList<Double> lastPrices;

    public MovingAverageBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public MovingAverageBuilder withPricingModel(PricingModel pricingModel) {
        this.pricingModel = pricingModel;
        return this;
    }

    public MovingAverageBuilder withAverage(Double average) {
        this.average = average;
        return this;
    }

    public MovingAverageBuilder withLastPrices(Collection<Double> lastPrices) {
        this.lastPrices =  new LinkedList<>(lastPrices);
        return this;
    }

    public MovingAverage build() {
        return new MovingAverage(this.id, this.pricingModel, this.average, this.lastPrices);
    }
}
