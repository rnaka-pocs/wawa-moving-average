package com.wawa.movingaverage.builder;

import com.wawa.movingaverage.bean.MovingAverageHistory;
import com.wawa.movingaverage.bean.PricingModel;

import java.time.LocalDateTime;

public class MovingAverageHistoryBuilder {

    private String id;
    private LocalDateTime createdOn;
    private PricingModel pricingModel;
    private Double average;

    public MovingAverageHistoryBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public MovingAverageHistoryBuilder withCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public MovingAverageHistoryBuilder withPricingModel(PricingModel pricingModel) {
        this.pricingModel = pricingModel;
        return this;
    }

    public MovingAverageHistoryBuilder withAverage(Double average) {
        this.average = average;
        return this;
    }

    public MovingAverageHistory build() {
        return new MovingAverageHistory(this.id, this.createdOn, this.pricingModel, this.average);
    }
}
