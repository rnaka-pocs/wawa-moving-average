package com.wawa.movingaverage.service;

import com.wawa.movingaverage.bean.Event;
import com.wawa.movingaverage.exception.NoContentException;

import javax.naming.NotContextException;

/**
 * Created by Naka on 6/1/19
 * watanabe.raphael@gmail.com
 */
public interface MovingAverageService {

    void process(Event event) throws NoContentException;
}
