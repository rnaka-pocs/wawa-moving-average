package com.wawa.movingaverage.service;

import com.wawa.movingaverage.bean.MovingAverage;

/**
 * Created by Naka on 6/2/19
 * watanabe.raphael@gmail.com
 */
public interface MovingAverageCalculationService {

    MovingAverage calculate(MovingAverage movingAverage);
}
