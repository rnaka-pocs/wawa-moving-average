package com.wawa.movingaverage.service.impl;

import com.wawa.movingaverage.service.CalculationService;
import org.springframework.stereotype.Service;

/**
 * Created by Naka on 5/31/19
 * watanabe.raphael@gmail.com
 */
@Service
public class CalculationSilverServiceImpl implements CalculationService {

    private final Double percentage = 1.05;

    @Override
    public Double calculate(Double price) {
        return price * percentage;
    }
}
