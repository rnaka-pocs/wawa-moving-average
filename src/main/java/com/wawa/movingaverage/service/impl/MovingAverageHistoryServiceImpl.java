package com.wawa.movingaverage.service.impl;

import com.wawa.movingaverage.bean.MovingAverage;
import com.wawa.movingaverage.bean.MovingAverageHistory;
import com.wawa.movingaverage.bean.PricingModel;
import com.wawa.movingaverage.builder.MovingAverageHistoryBuilder;
import com.wawa.movingaverage.exception.NoContentException;
import com.wawa.movingaverage.repository.MovingAverageHistoryRepository;
import com.wawa.movingaverage.service.MovingAverageHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Naka on 6/1/19
 * watanabe.raphael@gmail.com
 */
@Service
public class MovingAverageHistoryServiceImpl implements MovingAverageHistoryService {

    @Autowired
    private MovingAverageHistoryRepository movingAverageHistoryRepository;

    @Override
    public MovingAverageHistory create(MovingAverage movingAverage) {
        LocalDateTime now = LocalDateTime.now();
        now = now.truncatedTo(ChronoUnit.MINUTES);

        MovingAverageHistory movingAverageHistory = defineMovingAverageHistory(movingAverage, now);

        return movingAverageHistoryRepository.save(movingAverageHistory);
    }

    private MovingAverageHistory defineMovingAverageHistory(MovingAverage movingAverage, LocalDateTime now) {
        LinkedList<MovingAverageHistory> movingAverageHistories = movingAverageHistoryRepository.findByCreatedOnAndPricingModel(now, movingAverage.getPricingModel());

        MovingAverageHistory movingAverageHistory;

        if (movingAverageHistories == null || movingAverageHistories.size() == 0) {
            movingAverageHistory = new MovingAverageHistoryBuilder()
                    .withCreatedOn(now)
                    .withPricingModel(movingAverage.getPricingModel())
                    .build();
        } else {
            movingAverageHistory = movingAverageHistories.getLast();
            movingAverageHistories.removeLast();
            movingAverageHistoryRepository.deleteAll(movingAverageHistories);
        }

        movingAverageHistory.setAverage(movingAverage.getAverage());

        return movingAverageHistory;
    }

    @Override
    public List<MovingAverageHistory> findByPricingModel(String pricingModel) throws NoContentException {
        PricingModel pm = PricingModel.findByString(pricingModel);

        List<MovingAverageHistory> mah = movingAverageHistoryRepository.findByPricingModel(pm);
        if (mah == null || mah.size() == 0) {
            throw new NoContentException();
        }
        return mah;
    }
}
