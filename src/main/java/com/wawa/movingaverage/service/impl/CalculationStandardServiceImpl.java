package com.wawa.movingaverage.service.impl;

import com.wawa.movingaverage.service.CalculationService;
import org.springframework.stereotype.Service;

/**
 * Created by Naka on 5/31/19
 * watanabe.raphael@gmail.com
 */
@Service
public class CalculationStandardServiceImpl implements CalculationService {

    @Override
    public Double calculate(Double price) {
        return price;
    }
}
