package com.wawa.movingaverage.service.impl;

import com.wawa.movingaverage.bean.Event;
import com.wawa.movingaverage.bean.MovingAverage;
import com.wawa.movingaverage.bean.PricingModel;
import com.wawa.movingaverage.exception.InvalidRequestException;
import com.wawa.movingaverage.exception.NoContentException;
import com.wawa.movingaverage.factory.CalculationServiceFactory;
import com.wawa.movingaverage.repository.MovingAverageRepository;
import com.wawa.movingaverage.service.CalculationService;
import com.wawa.movingaverage.service.MovingAverageCalculationService;
import com.wawa.movingaverage.service.MovingAverageHistoryService;
import com.wawa.movingaverage.service.MovingAverageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.NotContextException;
import java.util.LinkedList;

/**
 * Created by Naka on 6/1/19
 * watanabe.raphael@gmail.com
 */
@Service
public class MovingAverageServiceImpl implements MovingAverageService {

    @Autowired
    private MovingAverageHistoryService movingAverageHistoryService;

    @Autowired
    private CalculationServiceFactory calculationServiceFactory;

    @Autowired
    private MovingAverageRepository movingAverageRepository;

    @Autowired
    private MovingAverageCalculationService movingAverageCalculationService;

    @Override
    public void process(Event event) throws InvalidRequestException {
        Double newPrice = calculatePrice(event);

        MovingAverage movingAverage = calculateMovingAverage(event, newPrice);

        save(movingAverage);
    }

    private Double calculatePrice(Event event) throws InvalidRequestException {
        PricingModel pricingModel = PricingModel.findByString(event.getPricing_model());

        if (pricingModel == null) {
            throw new InvalidRequestException("test");
        }

        CalculationService calculationService = this.calculationServiceFactory.getMovingAverage(pricingModel);

        return calculationService.calculate(event.getPrice());
    }

    private MovingAverage calculateMovingAverage(Event event, Double newPrice) {
        PricingModel pricingModel = PricingModel.findByString(event.getPricing_model());
        MovingAverage movingAverage = movingAverageRepository.findByPricingModel(pricingModel);

        if (movingAverage != null) {
            movingAverage.getLastPrices().add(newPrice);
        } else {
            movingAverage = new MovingAverage();
            movingAverage.setPricingModel(pricingModel);

            LinkedList<Double> lastPrices = new LinkedList<>();
            lastPrices.add(newPrice);
            movingAverage.setLastPrices(lastPrices);
        }

        movingAverage = movingAverageCalculationService.calculate(movingAverage);
        return movingAverage;
    }

    private void save(MovingAverage movingAverage) {
        movingAverageHistoryService.create(movingAverage);

        movingAverageRepository.save(movingAverage);
    }
}
