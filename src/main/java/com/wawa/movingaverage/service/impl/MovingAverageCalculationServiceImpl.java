package com.wawa.movingaverage.service.impl;

import com.wawa.movingaverage.bean.MovingAverage;
import com.wawa.movingaverage.service.MovingAverageCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by Naka on 6/2/19
 * watanabe.raphael@gmail.com
 */
@Service
public class MovingAverageCalculationServiceImpl implements MovingAverageCalculationService {

    @Value("${movingaverage.sizeLimit}")
    private int sizeLimit;

    public MovingAverage calculate(MovingAverage movingAverage) {
        while (movingAverage.getLastPrices().size() > this.sizeLimit) {
            movingAverage.getLastPrices().removeFirst();
        }

        Optional<Double> sum = movingAverage.getLastPrices().stream().reduce((a, b) -> a + b);

        if (!sum.isPresent()) {
            movingAverage.setAverage(0.0);
        } else {
            Double average = sum.get() / movingAverage.getLastPrices().size();
            movingAverage.setAverage(average);
        }

        return movingAverage;
    }
}
