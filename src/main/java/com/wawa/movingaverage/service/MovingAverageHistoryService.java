package com.wawa.movingaverage.service;

import com.wawa.movingaverage.bean.MovingAverage;
import com.wawa.movingaverage.bean.MovingAverageHistory;
import com.wawa.movingaverage.bean.PricingModel;

import java.util.List;

/**
 * Created by Naka on 6/1/19
 * watanabe.raphael@gmail.com
 */
public interface MovingAverageHistoryService {

    MovingAverageHistory create(MovingAverage movingAverage);

    List<MovingAverageHistory> findByPricingModel(String pricingModel);
}
