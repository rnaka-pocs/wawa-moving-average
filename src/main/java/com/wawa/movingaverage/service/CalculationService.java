package com.wawa.movingaverage.service;

/**
 * Created by Naka on 6/1/19
 * watanabe.raphael@gmail.com
 */
public interface CalculationService {

    Double calculate(Double price);
}
