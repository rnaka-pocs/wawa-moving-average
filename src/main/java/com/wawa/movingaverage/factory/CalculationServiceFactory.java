package com.wawa.movingaverage.factory;

import com.wawa.movingaverage.bean.PricingModel;
import com.wawa.movingaverage.service.CalculationService;
import com.wawa.movingaverage.service.impl.CalculationGoldServiceImpl;
import com.wawa.movingaverage.service.impl.CalculationStandardServiceImpl;
import com.wawa.movingaverage.service.impl.CalculationSilverServiceImpl;
import org.springframework.stereotype.Component;

@Component
public class CalculationServiceFactory {

    public CalculationService getMovingAverage(PricingModel pricingModel) {
        CalculationService calculationService = null;

        if (pricingModel != null) {
            switch (pricingModel) {
                case STANDARD:
                    calculationService = new CalculationStandardServiceImpl();
                    break;
                case GOLD:
                    calculationService = new CalculationGoldServiceImpl();
                    break;
                case SILVER:
                    calculationService = new CalculationSilverServiceImpl();
                    break;
            }
        }

        return calculationService;
    }
}
