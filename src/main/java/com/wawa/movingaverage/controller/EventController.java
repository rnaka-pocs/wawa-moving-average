package com.wawa.movingaverage.controller;

import com.wawa.movingaverage.bean.Event;
import com.wawa.movingaverage.exception.InvalidRequestException;
import com.wawa.movingaverage.service.MovingAverageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "events")
public class EventController {

    @Autowired
    private MovingAverageService movingAverageService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Event event) throws InvalidRequestException {
        movingAverageService.process(event);
    }

}
