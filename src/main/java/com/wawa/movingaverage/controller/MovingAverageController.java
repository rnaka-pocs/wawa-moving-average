package com.wawa.movingaverage.controller;

import com.wawa.movingaverage.bean.MovingAverageHistory;
import com.wawa.movingaverage.service.MovingAverageHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("moving-averages")
public class MovingAverageController {

    @Autowired
    private MovingAverageHistoryService movingAverageHistoryService;

    @RequestMapping(method = RequestMethod.GET, value = "/history/{pricing-model}")
    public List<MovingAverageHistory> listByPricingModel(@PathVariable("pricing-model") String pricingModel) {
        return movingAverageHistoryService.findByPricingModel(pricingModel);
    }
}
