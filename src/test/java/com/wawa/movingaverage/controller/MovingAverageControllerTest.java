package com.wawa.movingaverage.controller;

import com.wawa.movingaverage.bean.MovingAverageHistory;
import com.wawa.movingaverage.bean.PricingModel;
import com.wawa.movingaverage.builder.MovingAverageHistoryBuilder;
import com.wawa.movingaverage.exception.NoContentException;
import com.wawa.movingaverage.service.MovingAverageHistoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@WebMvcTest(MovingAverageController.class)
public class MovingAverageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovingAverageHistoryService movingAverageHistoryService;

    @Test
    public void given_when_then() throws Exception{
        // given
        final String pricingModel = "standard";
        List<MovingAverageHistory> mas = createThree();

        given(movingAverageHistoryService.findByPricingModel(pricingModel))
                .willReturn(mas);

        // when
        MockHttpServletResponse response =
                mockMvc.perform(get("/moving-averages/history/"+pricingModel))
                .andReturn()
                .getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    public void givenNoDocuments_whenMovingAveragesHistory_thenNoReturn() throws Exception {
        // given
        final String pricingModel = "asdfads";


        given(movingAverageHistoryService.findByPricingModel(pricingModel))
                .willThrow(NoContentException.class);

        // when
        MockHttpServletResponse response =
                mockMvc.perform(get("/moving-averages/history/"+pricingModel))
                        .andReturn()
                        .getResponse();

        // then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NO_CONTENT.value());
    }

    private List<MovingAverageHistory> createThree() {
        final PricingModel pricingModel = PricingModel.SILVER;
        MovingAverageHistory ma1 = new MovingAverageHistoryBuilder()
                .withAverage(230.2)
                .withCreatedOn(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES))
                .withId("faj23lf")
                .withPricingModel(pricingModel)
                .build();
        MovingAverageHistory ma2 = new MovingAverageHistoryBuilder()
                .withAverage(34.2)
                .withCreatedOn(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES).minusMinutes(10))
                .withId("2848fjdf")
                .withPricingModel(pricingModel)
                .build();
        MovingAverageHistory ma3 = new MovingAverageHistoryBuilder()
                .withAverage(2.1)
                .withCreatedOn(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES).minusMinutes(20))
                .withId("f34fgk3")
                .withPricingModel(pricingModel)
                .build();

        List<MovingAverageHistory> mas = new ArrayList<>();
        mas.add(ma1);
        mas.add(ma2);
        mas.add(ma3);
        return mas;
    }

}