package com.wawa.movingaverage.controller;

import com.wawa.movingaverage.bean.Event;
import com.wawa.movingaverage.exception.InvalidRequestException;
import com.wawa.movingaverage.service.MovingAverageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static com.wawa.movingaverage.controller.JSONHelper.asJsonString;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * Created by Naka on 6/2/19
 * watanabe.raphael@gmail.com
 */
@RunWith(SpringRunner.class)
@WebMvcTest(EventController.class)
public class EventControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovingAverageService movingAverageService;

    @Test
    public void givenEvent_whenPOST_thenHttpCREATED() throws Exception{
        // given
        Event event = new Event();
        event.setAttribute1("water");
        event.setCategory("blue");
        event.setId(123L);
        event.setPrice(1.50);
        event.setPricing_model("standard");

        Mockito.doNothing().when(movingAverageService).process(event);

        // when
        MockHttpServletResponse response = mockMvc.perform(post("/events")
                .content(asJsonString(event))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // then
        assertEquals("", HttpStatus.CREATED.value(), response.getStatus());
    }

    @Test
    public void givenInvalidEvent_whenPOST_thenHttpCREATED() throws Exception{
        // given
        Event event = new Event();

        Mockito.doThrow(InvalidRequestException.class).when(movingAverageService).process(Mockito.any());

        // when
        MockHttpServletResponse response = mockMvc.perform(post("/events")
                .content(asJsonString(event))
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        // then
        assertEquals("", HttpStatus.UNPROCESSABLE_ENTITY.value(), response.getStatus());
    }

}
