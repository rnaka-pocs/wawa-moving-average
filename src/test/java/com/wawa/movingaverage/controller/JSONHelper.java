package com.wawa.movingaverage.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

class JSONHelper {

    static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
