package com.wawa.movingaverage.service;

import com.wawa.movingaverage.service.impl.CalculationSilverServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class CalculationSilverServiceImplTest {

    @TestConfiguration
    static class CalculationSilverServiceImplTestContextConfiguration {

        @Bean
        public CalculationService calculationService() {
            return new CalculationSilverServiceImpl();
        }
    }

    @Autowired
    private CalculationService calculationSilverService;

    @Test
    public void givenPrice_whenCalculate_thenPricePlusPercentage() {
        // given
        final Double price = 10.0;
        final Double percentage = 1.05;

        // when
        Double newPrice = calculationSilverService.calculate(price);

        // then
        Double expectedPrice = price * percentage;
        assertEquals("NewPrice is wrong", expectedPrice, newPrice);
    }
}
