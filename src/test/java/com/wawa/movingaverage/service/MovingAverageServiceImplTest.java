package com.wawa.movingaverage.service;

import com.wawa.movingaverage.bean.Event;
import com.wawa.movingaverage.bean.MovingAverage;
import com.wawa.movingaverage.bean.PricingModel;
import com.wawa.movingaverage.builder.MovingAverageBuilder;
import com.wawa.movingaverage.exception.InvalidRequestException;
import com.wawa.movingaverage.factory.CalculationServiceFactory;
import com.wawa.movingaverage.repository.MovingAverageRepository;
import com.wawa.movingaverage.service.impl.CalculationStandardServiceImpl;
import com.wawa.movingaverage.service.impl.MovingAverageServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
public class MovingAverageServiceImplTest {

    @TestConfiguration
    static class MovingAverageServiceImplTestContextConfiguration {

        @Bean
        public MovingAverageService movingAverageService() {
            return new MovingAverageServiceImpl();
        }
    }

    @Autowired
    private MovingAverageService movingAverageService;

    @MockBean
    private MovingAverageHistoryService movingAverageHistoryService;

    @MockBean
    private CalculationServiceFactory calculationServiceFactory;

    @MockBean
    private MovingAverageRepository movingAverageRepository;

    @MockBean
    private MovingAverageCalculationService movingAverageCalculationService;


    @Before
    public void init() {
        final PricingModel pricingModel = PricingModel.STANDARD;
        Mockito.when(calculationServiceFactory.getMovingAverage(pricingModel))
                .thenReturn(new CalculationStandardServiceImpl());
    }

    @Test
    public void givenWithNoMovingAverage_whenProcess_thenSuccess() {
        // given
        Event event = new Event();
        event.setId(28348L);
        event.setCategory("blue");
        event.setPrice(384.5);
        event.setPricing_model("standard");

        Mockito.when(movingAverageRepository.findByPricingModel(PricingModel.findByString(event.getPricing_model())))
                .thenReturn(null);

        MovingAverage movingAverage = new MovingAverageBuilder()
                .withId("fa2f8sdfj2")
                .withPricingModel(PricingModel.findByString(event.getPricing_model()))
                .withAverage(event.getPrice())
                .withLastPrices(Arrays.asList(event.getPrice()))
                .build();

        Mockito.when(movingAverageCalculationService.calculate(movingAverage))
                .thenReturn(movingAverage);

        // when
        try {
            movingAverageService.process(event);
        } catch(Exception ex) {
            fail("Process call failed");
        }
    }

    @Test
    public void givenWithMovingAverage_whenProcess_thenSuccess() {
        // given
        Event event = new Event();
        event.setId(28348L);
        event.setCategory("blue");
        event.setPrice(384.5);
        event.setPricing_model("standard");

        MovingAverage movingAverage = new MovingAverageBuilder()
                .withId("fa2f8sdfj2")
                .withPricingModel(PricingModel.findByString(event.getPricing_model()))
                .withAverage(event.getPrice())
                .withLastPrices(Arrays.asList(event.getPrice()))
                .build();

        Mockito.when(movingAverageRepository.findByPricingModel(PricingModel.findByString(event.getPricing_model())))
                .thenReturn(movingAverage);

        Mockito.when(movingAverageCalculationService.calculate(movingAverage))
                .thenReturn(movingAverage);

        // when
        try {
            movingAverageService.process(event);
        } catch(Exception ex) {
            fail("Process call failed");
        }
    }

    @Test
    public void givenInvalidPricingModel_whenProcess_thenSuccess() {
        // given
        Event event = new Event();
        event.setId(28348L);
        event.setCategory("blue");
        event.setPrice(384.5);
        event.setPricing_model("asdfa");

        MovingAverage movingAverage = new MovingAverageBuilder()
                .withId("fa2f8sdfj2")
                .withPricingModel(PricingModel.findByString(event.getPricing_model()))
                .withAverage(event.getPrice())
                .withLastPrices(Arrays.asList(event.getPrice()))
                .build();

        Mockito.when(movingAverageRepository.findByPricingModel(PricingModel.findByString(event.getPricing_model())))
                .thenReturn(movingAverage);

        Mockito.when(movingAverageCalculationService.calculate(movingAverage))
                .thenReturn(movingAverage);

        // when
        try {
            movingAverageService.process(event);
            fail("Process call failed");
        } catch(Exception ex) {
            assertEquals("Invalid exception", InvalidRequestException.class, ex.getClass());
        }
    }
}
