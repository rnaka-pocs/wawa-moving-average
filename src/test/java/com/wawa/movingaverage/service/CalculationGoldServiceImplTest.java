package com.wawa.movingaverage.service;

import com.wawa.movingaverage.service.impl.CalculationGoldServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CalculationGoldServiceImplTest {

    @TestConfiguration
    static class CalculationGoldServiceImplTestContextConfiguration {

        @Bean
        public CalculationService calculationService() {
            return new CalculationGoldServiceImpl();
        }
    }

    @Autowired
    private CalculationGoldServiceImpl calculationGoldService;

    @Test
    public void givenPrice_whenCalculate_thenPricePlusPercentage() {
        // given
        final Double price = 10.0;
        final Double percentage = 1.10;

        // when
        Double newPrice = calculationGoldService.calculate(price);

        // then
        Double expectedPrice = price * percentage;
        assert(newPrice.equals(expectedPrice));
    }
}
