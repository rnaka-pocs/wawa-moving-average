package com.wawa.movingaverage.service;

import com.wawa.movingaverage.bean.MovingAverage;
import com.wawa.movingaverage.bean.MovingAverageHistory;
import com.wawa.movingaverage.bean.PricingModel;
import com.wawa.movingaverage.builder.MovingAverageBuilder;
import com.wawa.movingaverage.builder.MovingAverageHistoryBuilder;
import com.wawa.movingaverage.exception.NoContentException;
import com.wawa.movingaverage.repository.MovingAverageHistoryRepository;
import com.wawa.movingaverage.service.impl.MovingAverageHistoryServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@RunWith(SpringRunner.class)
public class MovingAverageHistoryServiceImplTest {

    @TestConfiguration
    static class MovingAverageServiceHistoryImplContextConfiguration {

        @Bean
        public MovingAverageHistoryService movingAverageHistoryService() {
            return new MovingAverageHistoryServiceImpl();
        }
    }

    @Autowired
    private MovingAverageHistoryService movingAverageHistoryService;

    @MockBean
    private MovingAverageHistoryRepository movingAverageHistoryRepository;

    @Test
    public void givenNotExistingDocument_whenCreate_thenDocumentCreated() {
        // given
        final PricingModel pricingModel = PricingModel.SILVER;
        final LinkedList<Double> lastPrices = new LinkedList<>(Arrays.asList(10.5, 57.0, 857.99));
        final Double average = (10.5+57.0+857.99)/3;
        final String id = "129380123fasd";

        MovingAverage movingAverage = new MovingAverageBuilder()
                .withPricingModel(pricingModel)
                .withLastPrices(lastPrices)
                .withAverage(average)
                .withId(id)
                .build();

        Mockito.when(movingAverageHistoryRepository.findByCreatedOnAndPricingModel(Mockito.any(), Mockito.any()))
                .thenReturn(null);

        MovingAverageHistory movingAverageHistory = new MovingAverageHistoryBuilder()
                .withAverage(823.3)
                .withCreatedOn(LocalDateTime.now())
                .withId("fakj348flad")
                .withPricingModel(PricingModel.SILVER)
                .build();

        Mockito.when(movingAverageHistoryRepository.save(Mockito.any()))
                .thenReturn(movingAverageHistory);

        // when
        MovingAverageHistory result = movingAverageHistoryService.create(movingAverage);

        // then
        assertNotNull("MovingAverageHistory not created", result);
        assertEquals("Invalid object returned", movingAverageHistory, result);
    }

    @Test
    public void givenExistingDocument_whenCreate_thenDocumentCreated() {
        // given
        final PricingModel pricingModel = PricingModel.GOLD;
        final LinkedList<Double> lastPrices = new LinkedList<>(Arrays.asList(15.8, 77.0, 1.99));
        final Double average = 544.56;
        final String id = "1294af123fasd";

        MovingAverage movingAverage = new MovingAverageBuilder()
                .withPricingModel(pricingModel)
                .withLastPrices(lastPrices)
                .withAverage(average)
                .withId(id)
                .build();

        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);

        MovingAverageHistory movingAverageHistory = new MovingAverageHistory();
        movingAverageHistory.setCreatedOn(now);
        movingAverageHistory.setId("1230192301982");
        movingAverageHistory.setPricingModel(pricingModel);
        movingAverageHistory.setAverage(130.98);

        LinkedList<MovingAverageHistory> mahs = new LinkedList<>();
        mahs.add(movingAverageHistory);

        Mockito.when(movingAverageHistoryRepository.findByCreatedOnAndPricingModel(Mockito.any(), Mockito.any()))
                .thenReturn(mahs);

        Mockito.when(movingAverageHistoryRepository.save(movingAverageHistory))
                .thenReturn(movingAverageHistory);

        // when
        MovingAverageHistory result = movingAverageHistoryService.create(movingAverage);

        // then
        assertNotNull("MovingAverageHistory not created", result);
        assertEquals("", movingAverageHistory, result);
    }

    @Test
    public void givenExistingMultipleDocuments_whenCreate_thenDocumentCreated() {
        // given
        final PricingModel pricingModel = PricingModel.GOLD;
        final LinkedList<Double> lastPrices = new LinkedList<>(Arrays.asList(15.8, 77.0, 1.99));
        final Double average = 544.56;
        final String id = "1294af123fasd";
        final int limit = 3;

        MovingAverage movingAverage = new MovingAverageBuilder()
                .withPricingModel(pricingModel)
                .withLastPrices(lastPrices)
                .withAverage(average)
                .withId(id)
                .build();

        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);

        LinkedList<MovingAverageHistory> mahs = new LinkedList<>();
        MovingAverageHistory movingAverageHistory;
        for (int i = 0; i < limit; i++) {
            movingAverageHistory = new MovingAverageHistory();
            movingAverageHistory.setCreatedOn(now);
            movingAverageHistory.setId("1230192301982"+i);
            movingAverageHistory.setPricingModel(pricingModel);
            movingAverageHistory.setAverage(130.98+i);

            mahs.add(movingAverageHistory);
        }

        Mockito.when(movingAverageHistoryRepository.findByCreatedOnAndPricingModel(Mockito.any(), Mockito.any()))
                .thenReturn(mahs);

        Mockito.doNothing().when(movingAverageHistoryRepository).deleteAll();

        MovingAverageHistory expectedResult = mahs.getLast();
        expectedResult.setAverage(average);
        Mockito.when(movingAverageHistoryRepository.save(expectedResult))
                .thenReturn(expectedResult);

        // when
        MovingAverageHistory result = movingAverageHistoryService.create(movingAverage);

        // then
        assertNotNull("MovingAverageHistory not created", result);
        assertEquals("Returned wrong ID", expectedResult.getId(), result.getId());
        assertEquals("Returned wrong CreatedOn", expectedResult.getCreatedOn(), result.getCreatedOn());
        assertEquals("Returned wrong PricingModel", expectedResult.getPricingModel(), result.getPricingModel());
        assertEquals("Returned wrong Average", average, result.getAverage());
    }


    @Test
    public void givenThreeObjectsToReturn_whenFindByPricingModel_thenReturnThree(){
        // given
        final PricingModel pricingModel = PricingModel.SILVER;
        List<MovingAverageHistory> expected = createThree();
        Mockito.when(movingAverageHistoryRepository.findByPricingModel(pricingModel))
            .thenReturn(expected);

        // when
        List<MovingAverageHistory> result = movingAverageHistoryService.findByPricingModel(pricingModel.name());

        // then
        for (MovingAverageHistory ma: expected) {
            assertTrue("MovingAverageHistory not found", result.stream().anyMatch(m -> Objects.equals(m, ma)));
        }
    }

    @Test
    public void givenNoObjectsToReturn_whenFindByPricingModel_thenThrowException() {
        // given
        final String pricingModel = "faljsdk";

        // when
        try {
            movingAverageHistoryService.findByPricingModel(pricingModel);
            fail("NoContentException not thrown");
        // then
        } catch(NoContentException ex) {
            assertEquals(NoContentException.class, ex.getClass());
        }
    }

    private List<MovingAverageHistory> createThree() {
        final PricingModel pricingModel = PricingModel.SILVER;
        MovingAverageHistory ma1 = new MovingAverageHistoryBuilder()
                .withAverage(11.2)
                .withCreatedOn(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES))
                .withId("312")
                .withPricingModel(pricingModel)
                .build();
        MovingAverageHistory ma2 = new MovingAverageHistoryBuilder()
                .withAverage(51.54)
                .withCreatedOn(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES).minusMinutes(7))
                .withId("asv234")
                .withPricingModel(pricingModel)
                .build();
        MovingAverageHistory ma3 = new MovingAverageHistoryBuilder()
                .withAverage(2.81)
                .withCreatedOn(LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES).minusMinutes(14))
                .withId("f34fgk3")
                .withPricingModel(pricingModel)
                .build();

        List<MovingAverageHistory> mas = new ArrayList<>();
        mas.add(ma1);
        mas.add(ma2);
        mas.add(ma3);
        return mas;
    }
}
