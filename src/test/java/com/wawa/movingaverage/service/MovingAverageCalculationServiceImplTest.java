package com.wawa.movingaverage.service;

import com.wawa.movingaverage.bean.MovingAverage;
import com.wawa.movingaverage.builder.MovingAverageBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedList;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MovingAverageCalculationServiceImplTest {

    @Autowired
    private MovingAverageCalculationService movingAverageCalculationService;

    @Value("${movingaverage.sizeLimit}")
    private int sizeLimit;

    @Test
    public void givenOnePrice_whenCalculate_thenSamePrice() {
        // given
        LinkedList<Double> lastPrices = new LinkedList<>();
        lastPrices.add(10.0);

        MovingAverage ma = new MovingAverageBuilder()
                .withLastPrices(lastPrices)
                .build();

        // when
        MovingAverage result = movingAverageCalculationService.calculate(ma);

        // then
        assert(result.getAverage().equals(10.0));
    }

    @Test
    public void givenTwoPrices_whenCalculate_thenAverageOfTwo() {
        // given
        LinkedList<Double> lastPrices = new LinkedList<>();
        lastPrices.add(10.0);
        lastPrices.add(5.0);

        MovingAverage ma = new MovingAverageBuilder()
                .withLastPrices(lastPrices)
                .build();

        // when
        MovingAverage result = movingAverageCalculationService.calculate(ma);

        // then
        assert(result.getAverage().equals(7.5));
    }

    @Test
    public void givenThreePrices_whenCalculate_thenAverageOfThree() {
        // given
        LinkedList<Double> lastPrices = new LinkedList<>();
        lastPrices.add(10.0);
        lastPrices.add(5.0);
        lastPrices.add(15.0);

        MovingAverage ma = new MovingAverageBuilder()
                .withLastPrices(lastPrices)
                .build();

        // when
        MovingAverage result = movingAverageCalculationService.calculate(ma);

        // then
        assert(result.getAverage().equals(10.0));
    }

    @Test
    public void givenFourPrices_whenCalculate_thenAverageOfTheLimit() {
        // given
        final double min = 0;
        final double max = 100;

        LinkedList<Double> lastPrices = new LinkedList<>();

        for (int i = 0; i < this.sizeLimit + 1; i++) {
            lastPrices.add(ThreadLocalRandom.current().nextDouble(min, max));
        }

        MovingAverage ma = new MovingAverageBuilder()
                .withLastPrices(lastPrices)
                .build();

        // when
        MovingAverage result = movingAverageCalculationService.calculate(ma);

        // then
        lastPrices.removeFirst();
        Optional<Double> sum = lastPrices.stream().reduce((a, b) -> a + b);
        Double expectedAverage = sum.get() / this.sizeLimit;

        assertEquals("Average is wrong", expectedAverage, result.getAverage());
    }
}
