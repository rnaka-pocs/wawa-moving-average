package com.wawa.movingaverage.service;

import com.wawa.movingaverage.service.impl.CalculationStandardServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Naka on 6/1/19
 * watanabe.raphael@gmail.com
 */
@RunWith(SpringRunner.class)
public class CalculationStandardServiceImplTest {

    @TestConfiguration
    static class CalculationStandardServiceImplTestContextConfiguration {

        @Bean
        public CalculationService calculationService() {
            return new CalculationStandardServiceImpl();
        }
    }

    @Autowired
    private CalculationStandardServiceImpl calculationStandardService;


    @Test
    public void givenPrice_whenCalculate_thenSamePrice() {
        final Double price = 10.0;

        Double newPrice = calculationStandardService.calculate(price);

        assert(newPrice.equals(price));
    }
}
