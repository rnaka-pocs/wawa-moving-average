package com.wawa.movingaverage.bean;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
public class PricingModelTest {

    @Test
    public void givenStandardString_whenFindByString_thenReturnPricingModelStandard() {
        // given
        final String pricingModelStr = "standard";

        // when
        PricingModel pricingModel = PricingModel.findByString(pricingModelStr);

        // then
        assertEquals("PricingModel is wrong", PricingModel.STANDARD, pricingModel);
    }

    @Test
    public void givenGoldString_whenFindByString_thenReturnPricingModelGold() {
        // given
        final String pricingModelStr = "gold";

        // when
        PricingModel pricingModel = PricingModel.findByString(pricingModelStr);

        // then
        assertEquals("PricingModel is wrong", PricingModel.GOLD, pricingModel);
    }

    @Test
    public void givenSilverString_whenFindByString_thenReturnPricingModelSilver() {
        // given
        final String pricingModelStr = "silver";

        // when
        PricingModel pricingModel = PricingModel.findByString(pricingModelStr);

        // then
        assertEquals("PricingModel is wrong", PricingModel.SILVER, pricingModel);
    }

    @Test
    public void givenInvalidString_whenFindByString_thenReturnNull() {
        // given
        final String pricingModelStr = "silveraaa";

        // when
        PricingModel pricingModel = PricingModel.findByString(pricingModelStr);

        // then
        assertNull("PricingModel is wrong", pricingModel);
    }
}
