package com.wawa.movingaverage.factory;

import com.wawa.movingaverage.bean.PricingModel;
import com.wawa.movingaverage.service.CalculationService;
import com.wawa.movingaverage.service.impl.CalculationGoldServiceImpl;
import com.wawa.movingaverage.service.impl.CalculationSilverServiceImpl;
import com.wawa.movingaverage.service.impl.CalculationStandardServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CalculationServiceFactoryTest {

    @TestConfiguration
    static class CalculationServiceFactoryTestContextConfiguration {

        @Bean
        public CalculationServiceFactory calculationServiceFactory() {
            return new CalculationServiceFactory();
        }
    }

    @Autowired
    private CalculationServiceFactory calculationServiceFactory;

    @Test
    public void givenStandard_whenGetMovingAverage_thenReturnStandardModel() {
        // given
        final PricingModel pricingModel = PricingModel.STANDARD;

        // when
        CalculationService calculationService = calculationServiceFactory.getMovingAverage(pricingModel);

        // then
        assert(calculationService.getClass().equals(CalculationStandardServiceImpl.class));
    }

    @Test
    public void givenSilver_whenGetMovingAverage_thenReturnSilverModel() {
        // given
        final PricingModel pricingModel = PricingModel.STANDARD.SILVER;

        // when
        CalculationService calculationService = calculationServiceFactory.getMovingAverage(pricingModel);

        // then
        assert(calculationService.getClass().equals(CalculationSilverServiceImpl.class));
    }

    @Test
    public void givenGold_whenGetMovingAverage_thenReturnGoldModel() {
        // given
        final PricingModel pricingModel = PricingModel.GOLD;

        // when
        CalculationService calculationService = calculationServiceFactory.getMovingAverage(pricingModel);

        // then
        assert(calculationService.getClass().equals(CalculationGoldServiceImpl.class));
    }

}
