package com.wawa.movingaverage.builder;

import com.wawa.movingaverage.bean.MovingAverage;
import com.wawa.movingaverage.bean.PricingModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
public class MovingAverageBuilderTest {

    @Test
    public void givenBasicData_whenBuild_thenObjectCreated() {
        // given
        final String id = "1fa134dfasdf";
        final Double average = 14.6;
        final LinkedList<Double> lastPrices = new LinkedList<>(Arrays.asList(15.1, 89.0, 27.3));
        final PricingModel pricingModel = PricingModel.GOLD;

        // when
        MovingAverage movingAverage = new MovingAverageBuilder()
                .withId(id)
                .withAverage(average)
                .withLastPrices(lastPrices)
                .withPricingModel(pricingModel)
                .build();

        // then
        assertNotNull("Object not generated", movingAverage);
        assertEquals("Id not set", id, movingAverage.getId());
        assertEquals("Average not set", average, movingAverage.getAverage());
        assertEquals("Pricing model not set", pricingModel, movingAverage.getPricingModel());
    }
}
