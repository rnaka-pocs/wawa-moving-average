package com.wawa.movingaverage.builder;

import com.wawa.movingaverage.bean.MovingAverage;
import com.wawa.movingaverage.bean.MovingAverageHistory;
import com.wawa.movingaverage.bean.PricingModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
public class MovingAverageHistoryBuilderTest {

    @TestConfiguration
    static class MovingAverageHistoryBuilderTestContextConfiguration {

        @Bean
        public MovingAverageHistoryBuilder movingAverageHistoryBuilder() {
            return new MovingAverageHistoryBuilder();
        }
    }

    @Autowired
    private MovingAverageHistoryBuilder movingAverageHistoryBuilder;

    @Test
    public void givenBasicData_whenBuild_thenObjectCreated() {
        // given
        final String id = "1fa134dfasdf";
        final Double average = 14.6;
        final LocalDateTime createdOn = LocalDateTime.now();
        final PricingModel pricingModel = PricingModel.GOLD;

        // when
        MovingAverageHistory movingAverageHistory = new MovingAverageHistoryBuilder()
                .withId(id)
                .withCreatedOn(createdOn)
                .withAverage(average)
                .withPricingModel(pricingModel)
                .build();

        // then
        assertNotNull("Object not generated", movingAverageHistory);
        assertEquals("Id not set", id, movingAverageHistory.getId());
        assertEquals("CreatedOn not set", createdOn, movingAverageHistory.getCreatedOn());
        assertEquals("Average not set", average, movingAverageHistory.getAverage());
        assertEquals("Pricing model not set", pricingModel, movingAverageHistory.getPricingModel());
    }
}
