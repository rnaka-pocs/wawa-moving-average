package com.wawa.movingaverage.repository;

import com.wawa.movingaverage.bean.MovingAverage;
import com.wawa.movingaverage.bean.PricingModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MovingAverageRepositoryIntegrationTest {

    @Autowired
    private MovingAverageRepository movingAverageRepository;

    @Test
    public void givenObject_whenSave_thenObjectSaved() {
        final PricingModel pricingModel = PricingModel.STANDARD;
        final Double average = 10.0;
        final LinkedList<Double> lastPrices = new LinkedList<>(Arrays.asList(10.0, 4.0, 11.0));

        // given
        MovingAverage movingAverage = new MovingAverage(pricingModel, average);
        movingAverage.setAverage(average);
        movingAverage.setPricingModel(pricingModel);
        movingAverage.setLastPrices(lastPrices);

        // when
        MovingAverage newMA = movingAverageRepository.save(movingAverage);

        // then
        assertNotNull("Object not saved", newMA.getId());
        assertEquals("Average not saved", newMA.getAverage(), movingAverage.getAverage());
        assertEquals("Pricing model not saved", newMA.getPricingModel(), movingAverage.getPricingModel());
        assertNotNull("LastPrices not saved", newMA.getLastPrices());
        assertEquals("Count LastPrices are wrong", newMA.getLastPrices(), movingAverage.getLastPrices());
        for (int i = 0; i < newMA.getLastPrices().size(); i++) {
            assertEquals("Price not saved", newMA.getLastPrices().get(i), movingAverage.getLastPrices().get(i));
        }

        movingAverageRepository.delete(newMA);
    }

    @Test
    public void givenPricingModel_whenFindByPricingModel_thenObjectReturned() {
        // given
        final PricingModel pricingModel = PricingModel.STANDARD;
        final Double average = 10.0;
        final LinkedList<Double> lastPrices = new LinkedList<>(Arrays.asList(10.0, 4.0, 11.0));

        MovingAverage movingAverage = new MovingAverage(pricingModel, average);
        movingAverage.setAverage(average);
        movingAverage.setPricingModel(pricingModel);
        movingAverage.setLastPrices(lastPrices);

        MovingAverage newMA = movingAverageRepository.save(movingAverage);

        // when
        MovingAverage result = movingAverageRepository.findByPricingModel(pricingModel);

        // then
        assertNotNull("Object not saved", newMA.getId());
        assertEquals("Average not saved", newMA.getAverage(), result.getAverage());
        assertEquals("Pricing model not saved", newMA.getPricingModel(), result.getPricingModel());
        assertNotNull("LastPrices not saved", newMA.getLastPrices());
        assertEquals("Count LastPrices are wrong", newMA.getLastPrices(), result.getLastPrices());
        for (int i = 0; i < newMA.getLastPrices().size(); i++) {
            assertEquals("Price not saved", newMA.getLastPrices().get(i), result.getLastPrices().get(i));
        }

        movingAverageRepository.delete(result);
    }

    @Test
    public void givenNotExistingDocument_whenFindByPricingModelNotExisting_thenNoObject() {
        // given
        final PricingModel pricingModel = PricingModel.STANDARD;

        // when
        MovingAverage result = movingAverageRepository.findByPricingModel(pricingModel);

        // then
        assertNull("Object found", result);
    }
}
