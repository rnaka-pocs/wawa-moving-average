package com.wawa.movingaverage.repository;

import com.wawa.movingaverage.bean.MovingAverageHistory;
import com.wawa.movingaverage.bean.PricingModel;
import com.wawa.movingaverage.builder.MovingAverageHistoryBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MovingAverageHistoryRepositoryIntegrationTest {

    @Autowired
    private MovingAverageHistoryRepository movingAverageHistoryRepository;

    @Test
    public void givenNoExistingObject_whenFindForNotExistingObject_thenObjectNull() {
        // given
        PricingModel pricingModel = PricingModel.GOLD;
        LocalDateTime now = LocalDateTime.now();

        // when
        LinkedList<MovingAverageHistory> result = movingAverageHistoryRepository.findByCreatedOnAndPricingModel(now, pricingModel);

        // then
        assertNotNull("List null", result);
        assertEquals("Items returned", 0, result.size());
    }

    @Test
    public void givenNewObject_whenSave_thenObjectSaved() {
        // given
        Double average = 14.75;
        PricingModel pricingModel = PricingModel.STANDARD;
        LocalDateTime now = LocalDateTime.now();

        MovingAverageHistory mah = new MovingAverageHistory();
        mah.setAverage(average);
        mah.setPricingModel(pricingModel);
        mah.setCreatedOn(now);

        // when
        MovingAverageHistory result = movingAverageHistoryRepository.save(mah);

        // then
        assertNotNull("Object not saved", result);
        assertNotNull("Id not generated", result.getId());
        assertEquals("Average not saved", result.getAverage(), average);
        assertEquals("CreatedOn not saved", result.getCreatedOn(), now);
        assertEquals("PricingModel not saved", result.getPricingModel(), pricingModel);
    }

    @Test
    public void givenExistingObject_whenFindByDateAndPricingModel_thenObjectReturned() {
        // given
        final Double average = 34.75;
        final PricingModel pricingModel = PricingModel.GOLD;
        final LocalDateTime now = LocalDateTime.now();

        MovingAverageHistory mah = new MovingAverageHistoryBuilder()
                .withCreatedOn(now)
                .withAverage(average)
                .withPricingModel(pricingModel)
                .build();

        movingAverageHistoryRepository.save(mah);

        // when
        LinkedList<MovingAverageHistory> resultList = movingAverageHistoryRepository.findByCreatedOnAndPricingModel(now, pricingModel);

        // then
        assertNotNull("Object not found", resultList);
        assertNotEquals("No results", 0, resultList.size());
        MovingAverageHistory result = resultList.get(0);
        assertNotNull("Id not returned", result.getId());
        assertEquals("Average not returned", result.getAverage(), average);
        assertEquals("CreatedOn not returned", result.getCreatedOn(), now);
        assertEquals("PricingModel not returned", result.getPricingModel(), pricingModel);
    }

    @Test
    public void givenExistingMultipleObjects_whenFindByDateAndPricingModel_thenListReturned() {
        // given
        final Double average = 34.75;
        final PricingModel pricingModel = PricingModel.GOLD;
        final LocalDateTime now = LocalDateTime.now();
        final int totalItems = 3;

        MovingAverageHistory mah;
        for (int i = 0; i < totalItems; i++) {
            mah = new MovingAverageHistoryBuilder()
                    .withCreatedOn(now)
                    .withAverage(average * i)
                    .withPricingModel(pricingModel)
                    .build();

            movingAverageHistoryRepository.save(mah);
        }

        // when
        LinkedList<MovingAverageHistory> resultList = movingAverageHistoryRepository.findByCreatedOnAndPricingModel(now, pricingModel);

        // then
        assertNotNull("Object not found", resultList);
        assertEquals("No results", totalItems, resultList.size());
    }

    @Test
    public void givenThreeMovingAverages_whenFindByPricingModel_thenReturnTwo() {
        // given
        List<MovingAverageHistory> mas = createThreeMovingAverages();
        final PricingModel pricingModel = PricingModel.STANDARD;

        // when
        List<MovingAverageHistory> result = movingAverageHistoryRepository.findByPricingModel(pricingModel);

        // then
        List<MovingAverageHistory> expected = mas.stream()
                .filter(m -> m.getPricingModel().equals(pricingModel))
                .collect(Collectors.toList());

        for (MovingAverageHistory ma : expected) {
            assertTrue("MovingAverageHistory not found", result.stream().anyMatch(m -> Objects.equals(m, ma)));
        }
    }

    private List<MovingAverageHistory> createThreeMovingAverages() {
        final PricingModel pricingModel1 = PricingModel.STANDARD;
        final PricingModel pricingModel2 = PricingModel.GOLD;
        final LocalDateTime createdOn = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);
        final Double average = 150.0;

        MovingAverageHistory ma1 = new MovingAverageHistoryBuilder()
                .withAverage(average)
                .withCreatedOn(createdOn)
                .withPricingModel(pricingModel1)
                .build();

        MovingAverageHistory ma2 = new MovingAverageHistoryBuilder()
                .withAverage(average)
                .withCreatedOn(createdOn.minusMinutes(5))
                .withPricingModel(pricingModel1)
                .build();

        MovingAverageHistory ma3 = new MovingAverageHistoryBuilder()
                .withAverage(average)
                .withCreatedOn(createdOn.minusMinutes(10))
                .withPricingModel(pricingModel2)
                .build();

        List<MovingAverageHistory> mas = new ArrayList<>();
        mas.add(movingAverageHistoryRepository.save(ma1));
        mas.add(movingAverageHistoryRepository.save(ma2));
        mas.add(movingAverageHistoryRepository.save(ma3));
        return mas;
    }
}
