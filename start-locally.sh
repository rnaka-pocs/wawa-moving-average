#!/bin/bash

echo
echo "*********************"
echo "Running mongo"
echo "*********************"
echo
docker run -d -p 27017:27017 mongo:4.0.3

echo
echo "*********************"
echo "Building the API"
echo "*********************"
echo
./mvnw clean install


echo
echo "*********************"
echo "Running the API"
echo "*********************"
echo
java -jar target/movingaverage-0.0.1-SNAPSHOT.jar
